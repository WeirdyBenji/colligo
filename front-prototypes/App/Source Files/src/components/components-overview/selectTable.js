import React, { Component } from 'react';
import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css'; 
import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import paginationFactory from 'react-bootstrap-table2-paginator';
import {
    Card,
    CardBody,
    CardHeader,
    FormCheckbox
} from "shards-react"
// import axios from 'axios';

class SelectTable extends Component {

  constructor(props) {
    super(props);

    this.state = {
      test: props.test,
      products: [{
          "name": "Jean",
      },
      {
          "name": "Louis",
      }],
      columns: [
      {
        dataField: 'name',
        text: 'Nom ',
        sort: true,
        filter: textFilter()
      }]
    }
    this.selectedAttendees = []
  }

//   componentDidMount() {
//     axios.get('http://localhost:4000/results')
//       .then(response => {
//         this.setState({
//           products: response.data
//         });
//       });
//   }
  
  selectRowProps = {mode: 'checkbox',
  selectionRenderer: ({ mode, checked, disabled, rowIndex }) => (
    <FormCheckbox checked={checked} name={rowIndex}/>
  ),
  selectionHeaderRenderer: ({ mode, checked, disabled, rowIndex }) => (
    <FormCheckbox checked={checked} name={rowIndex}/>
  ),
  onSelect: (row, isSelect, rowIndex, e) => {
    if (isSelect)
      this.selectedAttendees[rowIndex] = row;
    else
      delete this.selectedAttendees[rowIndex];

    this.setState({test: "test"});
  },
  onSelectAll: (isSelect, rows, e) => {
    if (isSelect)
      this.selectedAttendees = rows;
    else
      this.selectedAttendees = [];
}};

  render() {
    return (
    <Card>
        <CardHeader>
            <h4>Séléctionnez les participants</h4>
        </CardHeader>
    <CardBody>
      <div className="container">
        <BootstrapTable 
        striped
        selectRow = {this.selectRowProps}
        hover
        keyField='name'
        data={ this.state.products } 
        columns={ this.state.columns }
        filter={ filterFactory() } 
        pagination={ paginationFactory() }/>
      </div>
      </CardBody>
    </Card>
    );
  }
}

export default SelectTable;