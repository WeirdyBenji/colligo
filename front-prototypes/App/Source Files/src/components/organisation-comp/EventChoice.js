import React from "react";
import {
  Row,
  Col,
  Card,
  CardBody,
  Badge,
} from "shards-react";

class EventChoice extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        const {
            PostsListOne,
        } = this.props.tab;

        return (
            <Row>
            {PostsListOne.map((post, idx) => (
                <Col lg="3" md="6" sm="12" className="mb-4" key={idx}>
                <Card small className="card-post card-post--1">
                <a href="#" className="text-fiord-blue" onClick={this.step2}>
                    <div
                    className="card-post__image"
                    style={{ backgroundImage: `url(${post.backgroundImage})` }}
                    >
                    <Badge
                        pill
                        className={`card-post__category bg-${post.categoryTheme}`}
                    >
                        {post.category}
                    </Badge>
                    </div>
                    <CardBody>
                    <h5 className="card-title">
                        {post.title}
                    </h5>
                    <p className="card-text d-inline-block mb-3">{post.body}</p>
                    <span className="text-muted">{post.date}</span>
                    </CardBody>
                    </a>
                </Card>
                </Col>
            ))}
            </Row>
        );
    }
}

export default EventChoice;