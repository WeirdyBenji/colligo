import React from "react";
import PropTypes from "prop-types";
import {
  Card,
  CardHeader,
  CardBody,
  Button,
  ListGroup,
  ListGroupItem,
  Progress
} from "shards-react";


class props extends React.Component {

  constructor(props) {
      super(props);
      this.def = {
        name: "Laser-game actor",
        avatar: require("./../../images/content-management/laser-game.jpg"),
        address: "30, boulevarde de la Marquette, Toulouse",
        performanceReportTitle: "Rating",
        performanceReportValue: 74,
        metaTitle: "Description",
        metaValue:
          "Lorem ipsum dolor sit amet consectetur adipisicing elit. Odio eaque, quidem, commodi soluta qui quae minima obcaecati quod dolorum sint alias, possimus illum assumenda eligendi cumque?"
      };
      this.def.name = props.name;
      this.def.address = props.address;
      this.def.avatar = props.avatar;
      this.def.metaValue = props.metaValue
  }

  render() {
    return (
      <Card small className="mb-4 pt-3">
        <CardHeader className="border-bottom text-center">
            <div
              className="card-post__image"
              style={{backgroundImage: `url(${this.def.avatar})`}}
            />
        </CardHeader>
        <CardBody className="text-center">
        <h4 className="mb-0">{this.def.name}</h4>
          <span className="text-muted d-block mb-2">{this.def.address}</span>
          <Button pill outline size="sm" className="mb-2">
            Website
          </Button>
        </CardBody>
        <ListGroup flush>
          <ListGroupItem className="px-4">
            <div className="progress-wrapper">
              <strong className="text-muted d-block mb-2">
                {this.def.performanceReportTitle}
              </strong>
              <Progress
                className="progress-sm"
                value={this.def.performanceReportValue}
              >
                <span className="progress-value">
                  {this.def.performanceReportValue}%
                </span>
              </Progress>
            </div>
          </ListGroupItem>
          <ListGroupItem className="p-4">
            <strong className="text-muted d-block mb-2">
              {this.def.metaTitle}
            </strong>
            <span>{this.def.metaValue}</span>
          </ListGroupItem>
        </ListGroup>
      </Card>
    );
  }
}

export default props;
