import React, { Component } from 'react';
import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css'; 
import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import paginationFactory from 'react-bootstrap-table2-paginator';
import {
    Card,
    CardBody,
    CardHeader,
    FormCheckbox,
    Button
} from "shards-react"
import axios from 'axios';

class SelectTable extends Component {

  constructor(props) {
    super(props);

    this.state = {
      users: [{
            "email": "l@l.l",
            'delete': <Button size="sm" className="mb-2 mr-1" theme="danger" name="0"><i class="material-icons">clear</i></Button>
        },
        {
            "email": "l@l.l",
            'delete': <Button size="sm" className="mb-2 mr-1" theme="danger" name="1"><i class="material-icons">clear</i></Button>
        }
      ]
    }

    this.columns = [
      {
        dataField: 'email',
        text: 'Email ',
        sort: true,
        filter: textFilter()
      },
      {
        dataField: 'delete',
        text: '',
        style: {width:"5%"}
      }]
  }

  updateUsers() {
    let buf = [];
     axios.get(`http://colligo.ddns.net/users`)
      .then(res => {
        const response = res.data;
        response.map((element) => {
            buf.push({
              email: element.email,
              "delete": <Button size="sm" name={element.email} className="mb-2 mr-1" theme="danger" name="1"><i class="material-icons">clear</i></Button>
            });
        });
        this.setState({users: buf});
      })
  }

  componentDidMount() {
    this.updateUsers();
  }
  
  render() {
    return (
      <div className="container">
        <BootstrapTable 
        striped
        hover
        keyField='id' 
        data={ this.state.users } 
        columns={ this.columns }
        filter={ filterFactory() } 
        pagination={ paginationFactory() }/>
      </div>
    );
  }
}

export default SelectTable;