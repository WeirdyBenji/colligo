export default function() {
  return [
    {
      title: "Vos événements",
      htmlBefore: '<i class="material-icons">vertical_split</i>',
      to: "/events",
    },
    {
      title: "Organisation",
      htmlBefore: '<i class="material-icons">forum</i>',
      to: "/organize",
    },
    {
      title: "Votre entreprise",
      htmlBefore: '<i class="material-icons">business</i>',
      to: "/enterprise",
    },
  ];
}
