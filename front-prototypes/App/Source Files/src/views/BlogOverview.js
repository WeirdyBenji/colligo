import React from "react";
import PropTypes from "prop-types";
import { Container, Row, Col,  Card, CardBody, CardHeader, ListGroup, ListGroupItem, Badge} from "shards-react";
import axios from "axios";
import moment from "moment";

import PageTitle from "./../components/common/PageTitle";
class EventsOverview extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      allEvents: []
    };
  }

  componentDidMount() {
    let buf = [];
    axios.get(`http://colligo.ddns.net/activity`)
      .then(res => {
        const response = res.data;
        console.log(response)
        response.map((element) => {
          if (element.title !== "-1") {
            buf.push({
              image: element.image,
              activity: element.title,
              actor: element.host,
              address: element.hostAddress,
              date: moment(new Date(null).setSeconds(element.date._seconds)).format('D/M/Y'),
              type: element.isPrivate ? "publique" : "privé",
              validated: element.state
            });
         }
        });
        this.setState({allEvents: buf});
      })
  }

  render() {
    const allEvents = this.state.allEvents;

    return(
      <Container fluid className="main-content-container px-4">
        {/* Page Header */}
        <Row noGutters className="page-header py-4">
          <PageTitle title="Vos événements" subtitle="Événements" className="text-sm-left mb-3" />
        </Row>

        {/* Small Stats Blocks */}
        <Row>
          {allEvents.length <= 0 ? <Col>Vous n'avez aucun événement de prévu</Col> :
          allEvents.map((event, idx) => (
            <Col lg="3" className="col-lg mb-4" key={idx}>
              <Card md="3">
                  <div
                      className="card-post__image"
                      style={{ backgroundImage: `url(${event.image})` }}
                  >
                </div>
                <CardBody>
                <div className="text-center">
                  <h4>{event.activity}</h4>
                  </div>
                <Col>
                    <ListGroup flush>
                      <ListGroupItem className="p-4">
                        <h6>Partenaire :</h6>
                        <span className="text-muted d-block mb-2">{event.actor}</span>
                        <h6>Addresse :</h6>
                        <span className="text-muted d-block mb-2">{event.address}</span>
                        <h6>Date :</h6>
                        <span className="text-muted d-block mb-2">{event.date}</span>
                        <h6>Type d'événement :</h6>
                        <span className="text-muted d-block mb-2">{event.type}</span>
                        <h6>Etat :</h6>
                        <Badge
                        pill
                        className={`float-right bg-${event.validated === 0 ? "warning" : event.validated === -1 ? "danger" : "success"}`}
                      >
                        {event.validated === 0 ? "En cours" : event.validated === -1 ? "Refusé" : "Validé"}
                      </Badge>
                      </ListGroupItem>
                    </ListGroup>
                  </Col>
                  <Col md="6">
                    
                  </Col>
                </CardBody>
              </Card>
            </Col>
          ))}
        </Row>
      </Container>
    )};
  }

EventsOverview.propTypes = {
  /**
   * The small stats dataset.
   */
  allEvents: PropTypes.array
};

EventsOverview.defaultProps = {
  allEvents: [
    {
      image: require("../images/content-management/laser-game.jpg"),
      activity: "Laser-game",
      actor: "Laser-game Toulouse",
      address: "30, boulevard de la marquette, Toulouse",
      date: "27/12/2020",
      type: "publique",
      validated: 0
    },
    {
      image: require("../images/content-management/escape-game.jpg"),
      activity: "Escape-game",
      actor: "Escape-game Toulouse",
      address: "30, boulevard de la marquette, Toulouse",
      date: "27/12/2020",
      type: "publique",
      validated: 1
    },
    {
      image: require("../images/content-management/escape-game.jpg"),
      activity: "Escape-game",
      actor: "Escape-game Toulouse",
      address: "30, boulevard de la marquette, Toulouse",
      date: "27/12/2020",
      type: "privé",
      validated: 2
    },
  ]
};

export default EventsOverview;
