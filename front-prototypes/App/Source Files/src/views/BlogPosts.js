/* eslint jsx-a11y/anchor-is-valid: 0 */

import React from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  CardHeader,
  Badge,
  Form,
  Progress,
  FormInput,
  ListGroup,
  ListGroupItem,
  Button,
  FormCheckbox
} from "shards-react";

import moment from 'moment'
import PageTitle from "../components/common/PageTitle";
import ActorDetail from "../components/user-profile-lite/UserDetails";
import SelectTable from "../components/components-overview/selectTable";
import EventCalendar from "../components/EventCalendar";
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css'; 
import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import paginationFactory from 'react-bootstrap-table2-paginator';
import axios from "axios"
import  { Redirect } from 'react-router-dom'

let dates;
class BlogPosts extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      steps: 0,
      activity: "",
      nameActor: "",
      imageActor: "",
      redirect: 0,
      descriptionActor: "",
      maxPeople: "",
      title: "Choisissez une activité",
      private: false,
      PostsListOne: [],
      PostsListTwo: [],
      allActors: [],
      products: [],
      columns: [
      {
        dataField: 'email',
        text: 'Email ',
        sort: true,
        filter: textFilter()
      }]
    };
    this.selectedAttendees = []

    this.PostsListTwo = {
      // First list of posts.
      PostsListTwo: [
        {
          backgroundImage: require("../images/content-management/velo.jpg"),
          category: "Sport",
          categoryTheme: "success",
          title: "Velo Acteur",
          body:
            "Cet acteur est le meilleurs acteur de vélo dans le monde",
          comment: "Près de chez vous"
        },
        {
          backgroundImage: require("../images/content-management/laser-game.jpg"),
          category: "In-room entertainment",
          categoryTheme: "dark",
          title: "Laser-game Toulouse",
          body:
            "Cet acteur est le meilleurs laser-game dans le monde",
          comment: "Près de chez vous"
        },
        {
          backgroundImage: require("../images/content-management/escape-game.jpg"),
          category: "In-room entertainment",
          categoryTheme: "dark",
          title: "Escape-game Compans",
          body:
            "Cet acteur est le meilleurs escape game dans le monde",
          comment: "Près de chez vous"
        }
      ],
    };
  }

  selectRowProps = {mode: 'checkbox',
  selectionRenderer: ({ mode, checked, disabled, rowIndex }) => (
    <FormCheckbox checked={checked} name={rowIndex}/>
  ),
  selectionHeaderRenderer: ({ mode, checked, disabled, rowIndex }) => (
    <FormCheckbox checked={checked} name={rowIndex}/>
  ),
  onSelect: (row, isSelect, rowIndex, e) => {
    if (isSelect)
      this.selectedAttendees[rowIndex] = row;
    else
      delete this.selectedAttendees[rowIndex];
  },
  onSelectAll: (isSelect, rows, e) => {
    if (isSelect)
      this.selectedAttendees = rows;
    else
      this.selectedAttendees = [];
}};


  step0() {
    this.setState({steps: 0, event: "", title: "Choisissez une activité"})
  }
  step1(acti) {
    let buf = [];
    console.log(this.state.allActors);
    if (acti !== "") {
      this.state.allActors.filter((e) => e.category === acti).map((element) => {
        buf.push(element);
      })
      this.setState({steps:1, title: "Choisissez votre partenaire", PostsListTwo: buf, activity: acti});
    }
    else {
      this.setState({steps:1, title: "Choisissez votre partenaire"});
    }
  }
  step2(post) {
    if (post === "") {
      this.setState({steps:2, event: this.state.nameActor, title: "Préparez votre évènement"})
    }
    else {
      this.setState({steps:2, event: post.title, addressActor:post.address, nameActor: post.title, imageActor:post.backgroundImage, descriptionActor: post.body, title: "Préparez votre évènement"})
    }
  }
  step3() {
    this.setState({steps:3, event: "Séléctionner une date", title: "Préparez votre évènement"})
  }

  activitySubmit(acti) {
    console.log(moment(new Date(acti.start)).format('D/M/Y-H/m/S'))
    dates = acti;
  }

  onValidation(state, selectedAttendees) {

    let buf = []
    selectedAttendees.map((element) => {
      buf.push(element.email);
    })
    console.log({category: state.activity, date: dates.start, description: "For sports", host: state.nameActor,
    hostAddress: state.addressActor, hostProficiency: state.activity, image: state.PostsListOne[0].backgroundImage, isPrivate: state.private,
    maxParticipants: state.maxPeople === "" ? 0 : state.maxPeople, numberParticipants: selectedAttendees.length, emails: buf, state: 0, theme: "dark",
    title: state.activity, type: "Sport"})
    axios.post("http://colligo.ddns.net/activity", {category: state.activity, date: dates.start, description: "For sports", host: state.nameActor,
      hostAddress: state.addressActor, hostProficiency: state.activity, image: state.PostsListOne[0].backgroundImage, isPrivate: state.private,
      maxParticipants: state.maxPeople === "" ? 0 : state.maxPeople, numberParticipants: selectedAttendees.length, emails: buf, state: 0, theme: "dark",
      title: state.activity, type: "Sport"}).then (res => {
          this.setState({redirect: true});
    })
    console.log(dates);
    console.log(state.private);
    console.log(selectedAttendees);
    console.log(state.nameActor);
    console.log(state.addressActor);
  }


  componentDidMount() {
    let bufi = [];
    axios.get(`http://colligo.ddns.net/users`)
     .then(res => {
       const response = res.data;
       response.map((element) => {
           bufi.push({
             email: element.email,
           });
       });
       this.setState({products: bufi});
     })


    let buf = [];
    axios.get(`http://colligo.ddns.net/proficiency`)
    .then(res => {
      const response = res.data;
      console.log(response)
      response.map((element) => {
          buf.push({
            backgroundImage: element.image,
            category: element.type,
            categoryTheme: element.theme,
            title: element.name,
            body: element.description,
            comment: ""
          });
      });
      this.setState({PostsListOne: buf});
    })

    let buff = [];
    axios.get(`http://colligo.ddns.net/host`)
    .then(res => {
      const response = res.data;
      console.log(response)
      response.map((element) => {
          buff.push({
            backgroundImage: element.image,
            category: element.proficiency,
            categoryTheme: "success",
            title: element.name,
            address: element.address,
            body: element.description,
            comment: ""
          });
      });
      this.setState({allActors: buff});
    })
  }
  render() {
    return (
      <Container fluid className="main-content-container px-4">
        {/* Page Header */}
        {this.state.redirect ? <Redirect  to='/events'/> : ""}
        <Row noGutters className="page-header py-4">
          <Col md="10">
            <PageTitle sm="4" title={this.state.title} subtitle="Organisation" className="text-sm-left" />
          </Col>
          <Col md="2">
            {this.state.steps > 0 ? <Button theme="danger" onClick={() => this.step0()}>Annuler</Button> : ""}
          </Col>
        </Row>
        {this.state.steps === 0 ?
        (<div>
        <Row>
          {this.state.PostsListOne.map((post, idx) => (
            <Col lg="3" md="6" sm="12" className="mb-4" key={idx}>
              <Card small className="card-post card-post--1">
              <a href="#" className="text-fiord-blue" onClick={() => this.step1(post.title)}>
                <div
                  className="card-post__image"
                  style={{ backgroundImage: `url(${post.backgroundImage})` }}
                >
                  <Badge
                    pill
                    className={`card-post__category bg-${post.categoryTheme}`}
                  >
                    {post.category}
                  </Badge>
                </div>
                <CardBody>
                  <h5 className="card-title">
                      {post.title}
                  </h5>
                  <p className="card-text d-inline-block mb-3">{post.body}</p>
                  <span className="text-muted">{post.comment}</span>
                </CardBody>
                </a>
              </Card>
            </Col>
          ))}
    </Row></div>)
      : this.state.steps === 1 ? (<div>
        <Row>
          {this.state.PostsListTwo.map((post, idx) => (
            <Col lg="3" md="6" sm="12" className="mb-4" key={idx}>
              <Card small className="card-post card-post--1">
              <a href="#" className="text-fiord-blue" onClick={() => this.step2(post)}>
                <div
                  className="card-post__image"
                  style={{ backgroundImage: `url(${post.backgroundImage})` }}
                >
                  <Badge
                    pill
                    className={`card-post__category bg-${post.categoryTheme}`}
                  >
                    {post.category}
                  </Badge>
                </div>
                <CardBody>
                  <h5 className="card-title">
                      {post.title}
                  </h5>
                  <p className="card-text d-inline-block mb-3">{post.body}</p>
                  <span className="text-muted">{post.comment}</span>
                </CardBody>
                </a>
              </Card>
            </Col>
          ))}
        </Row>
        </div>): 
        (<div>
           <Card small className="mb-4">
            <CardHeader className="border-bottom">
              <h6 className="m-0">{this.state.event}</h6>
            </CardHeader>
            <ListGroup flush>
              <ListGroupItem className="p-3">
                {this.state.steps === 2 ? (
                <Row>
                  <Col>
                    <Form>
                      <Row>
                        <Col md="6" className="form-group">
                          {this.state.private === true ? (
                            <div>
                                <Card>
                                    <CardHeader>
                                        <h4>Séléctionnez les participants</h4>
                                    </CardHeader>
                                <CardBody>
                                  <div className="container">
                                    <BootstrapTable 
                                    striped
                                    selectRow = {this.selectRowProps}
                                    hover
                                    keyField='email'
                                    data={ this.state.products } 
                                    columns={ this.state.columns }
                                    filter={ filterFactory() } 
                                    pagination={ paginationFactory() }/>
                                  </div>
                                  </CardBody>
                                </Card>
                                <br />
                                <Col>
                                  <Button theme="secondary" onClick={() => {this.setState({private: false}); this.selectedAttendees = []}}>Passer l'événement en publique</Button>
                                </Col>
                            </div>) : (
                            <Card style={{height:"100%"}}>
                              <CardHeader>
                                <h4>Cet événement est publique</h4>
                              </CardHeader>
                              <CardBody>
                                <Col>
                                  <Row>
                                  <div>
                                    Un événement publique conviera l'ensemble de vos employés, vous pouvez cependant limiter le nombre de places maximum,
                                    auquel cas vous pourrez choisir les participants si trop de personne répondent présent.<br />
                                    Nombre de personne maximum: (laissez vide pour ne pas mettre de limite)</div>
                                    <FormInput
                                      id="maxNumberOfPpl"
                                      type="number"
                                      placeholder="Indéfini"
                                      value={this.state.maxPeople}
                                      onChange={(event) => {event.target.value >= 0 && this.setState({maxPeople: event.target.value})}}
                                      style={{width: "50%"}}
                                    />
                                  </Row>
                                  <br />
                                  <Row>
                                    Vous pouvez passer l'événement en privé pour choisir les participants.
                                  </Row>
                                  <br />
                                  <Row>
                                    <Button theme="secondary" onClick={() => this.setState({private: true})}>Passer l'événement en privé</Button>
                                  </Row>
                                </Col>
                              </CardBody>
                            </Card>
                          )}
                        </Col>
                        <Col md="6">
                          <ActorDetail address={this.state.addressActor }name={this.state.nameActor} avatar={this.state.imageActor} metaValue={this.state.descriptionActor} />
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                        <Button theme="accent" onClick={() => this.step1("")}>Précédent</Button>
                        </Col>
                        <Col md="1">
                          <Button theme="accent" onClick={() => this.step3()}>Suivant</Button>
                        </Col>
                      </Row>
                    </Form>
                  </Col>
                </Row>
                ) : (
                  <CardBody>
                    <Row>
                      <EventCalendar isCompanyUser activityName={this.state.activity} onActivitySelect={this.activitySubmit}/>
                    </Row>
                    <br />
                    <Row>
                      <Col>
                        <Button theme="accent" onClick={() => this.step2("")}>Précédent</Button>
                      </Col>
                      <Col md="1">
                        <Button theme="success" onClick={() => this.onValidation(this.state, this.selectedAttendees)}>Valider</Button>
                      </Col>
                    </Row>
                  </CardBody>
                )}
              </ListGroupItem>
            </ListGroup>
          </Card>
      </div>)}
        <div className="progress-wrapper">
          <h6>
            Steps
          </h6>
          <Progress
            className="progress-sm"
            value={this.state.steps * 30}
          >
            <span className="progress-value">
              {this.state.steps * 30}%
            </span>
          </Progress>
        </div>
      </Container>
    );
  }
}

export default BlogPosts;
