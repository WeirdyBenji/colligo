import React from "react";
import { Container, Row, Col, CardBody, Card, CardHeader, Form, FormFeedback, Button, FormInput } from "shards-react";

import PageTitle from "../components/common/PageTitle";
import UserDetails from "../components/user-profile-lite/enterpriseDetails";
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css'; 
import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import paginationFactory from 'react-bootstrap-table2-paginator';
import Axios from "axios";

class UserProfileLite extends React.Component {

  constructor (props) {
    super(props);

    this.state = {
      password: "",
      email: "",
      isvalid:false,
      isinvalid:false,
      error:"",
      users: [
      ]
    };

    this.columns = [
      {
        dataField: 'email',
        text: 'Email ',
        sort: true,
        filter: textFilter()
      }]
  }

  updateUsers() {
    let buf = [];
     Axios.get(`http://colligo.ddns.net/users`)
      .then(res => {
        const response = res.data;
        response.map((element) => {
            buf.push({
              email: element.email,
            });
        });
        this.setState({users: buf});
      })
  }

  componentDidMount() {
    this.updateUsers();
  }

  onSubmit(e, password, email) {
    e.preventDefault();
    Axios.post(`http://colligo.ddns.net/register`, {email: email, password: password})
    .then(res => {
        console.log(res)
        if (res.data === "Succesfully registered!") {
          this.setState({password:"", email:"", isvalid:true, isinvalid:false});
          this.updateUsers();
        }
        else {
          this.setState({isinvalid:true, isvalid:false, error:res.data.message});
        }
    });
    return false;
  }

  render() {
    return(
  <Container fluid className="main-content-container px-4">
    <Row noGutters className="page-header py-4">
      <PageTitle title="Entreprise" subtitle="Votre entreprise" md="12" className="ml-sm-auto mr-sm-auto" />
    </Row>
    <Row>
      <Col lg="4">
        <UserDetails />
      </Col>
      <Col lg="8">
        <Card small className="mb-4">
        <CardHeader className="border-bottom">
          <h6 className="m-0">Liste de vos employés</h6>
        </CardHeader>
        <CardBody>
          <BootstrapTable 
          striped
          hover
          keyField='email' 
          data={ this.state.users } 
          columns={ this.columns }
          filter={ filterFactory() } 
          pagination={ paginationFactory() }/>
        </CardBody>
      </Card>
        <Card>
          <CardHeader className="border-bottom">
            <h5>Enregistrer un membre</h5>
          </CardHeader>
          <CardBody>
            <Form onSubmit={(e) => {this.onSubmit(e, this.state.password, this.state.email)}}>
              <Row form>
                <Col md="6" className="form-group">
                  <FormInput
                    value={this.state.email}
                    placeholder="Email"
                    type="email"
                    required
                    onChange={(e) => {this.setState({email: e.target.value})}}
                  />
                </Col>
                <Col md="6" className="form-group">
                  <FormInput
                    value={this.state.password}
                    placeholder="Password"
                    type="password"
                    required
                    valid={this.state.isvalid}
                    invalid={this.state.isinvalid}
                    onChange={(e) => {this.setState({password: e.target.value})}}
                  />
                  <FormFeedback valid>Utilisateur enregistré</FormFeedback>
                  <FormFeedback invalid>Il y a eu une erreur: {this.state.error}</FormFeedback>
                </Col>
                <Col>
                <Button color="primary" type="submit" className="float-right form-group">Enregistrer</Button>
                </Col>
              </Row>
            </Form>
          </CardBody>
        </Card>
      </Col>
    </Row>
  </Container>
    );}
};

export default UserProfileLite;
