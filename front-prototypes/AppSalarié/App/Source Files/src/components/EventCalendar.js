import React from "react";
import { Calendar, momentLocalizer } from 'react-big-calendar'
import moment from 'moment'
import "react-big-calendar/lib/css/react-big-calendar.css";

const localizer = momentLocalizer(moment)

/**
 * 3 props:
 * isCompanyUser | BOOLEAN
 * activityName | STRING (only if isCompanyUser is true)
 * onActivitySelect | FUNCTION (only if isCompanyUser is true)
 *   it gives a slotInfo object in argument:
 *      {
 *        start: Date()
 *        end: Date()
 *        slots: [0: Date(), 1: Date(), ...]
 *      }
 */

class EventCalendar extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      eventsList: [],
      isSelectable: this.props.isCompanyUser,
      minTime: new Date(),
      maxTime: new Date(),
    }
    this.onSelecting = this.onSelecting.bind(this)
  }

  onSelecting = (slotInfo) => {
    this.setState({
      eventsList: [{
          title: this.props.activityName,
          start: slotInfo.start,
          end: slotInfo.end,
      }]
    })
    this.props.onActivitySelect(slotInfo)
  }

  componentDidMount() {
    this.state.minTime.setHours(8,0,0);
    this.state.maxTime.setHours(23,0,0);
  }

  render() {
    return (
      <Calendar
        localizer={localizer}
        events={this.state.eventsList}
        defaultDate={new Date()}
        defaultView="week"
        startAccessor="start"
        endAccessor="end"
        selectable={this.state.isSelectable}
        min={this.state.minTime}
        max={this.state.maxTime}
        onSelectSlot={this.onSelecting}
        style={{ height: "80vh",  width: '100%'}}
      />
    );
  }
}

export default EventCalendar;