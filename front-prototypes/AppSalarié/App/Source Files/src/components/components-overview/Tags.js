import React, { Component } from 'react';
import { Row, Col, Button, Card } from "shards-react";
import PageTitle from "../common/PageTitle";

class Tags extends Component {
    constructor(props) {
        super(props);

        this.state = {
            tags: props.tags
        };
    }


    render () {
        return (
            <div>
                <Row noGutters className="page-header py-4">
                    <PageTitle
                    md="12"
                    className="ml-sm-auto mr-sm-auto"
                    title="Mes Préférences"
                    />
                </Row>

                <Row className="mb-2">
                    {this.props.tags.map(tag => (
                        <Col className="mb-4">
                            <Card>
                                <div
                                    className={`bg-${tag.color} text-white text-center rounded p-3`}
                                    style={{boxShadow: "inset 0 0 5px rgba(0,0,0,.2)"}}>
                                    {tag.name}
                                </div>
                            </Card>
                        </Col>
                    ))}
                </Row>

                <Row noGutters className="page-header py-4">
                    <PageTitle
                    md="12"
                    className="ml-sm-auto mr-sm-auto"
                    title="Mes choix"
                    />
                </Row>

                <Row>
                    <Col>
                        <Button outline theme="primary" className="mb-2 mr-1">
                            Vélo
                        </Button>
                        <Button outline theme="secondary" className="mb-2 mr-1">
                            Laser-game
                        </Button>
                        <Button outline theme="success" className="mb-2 mr-1">
                            Escape Games
                        </Button>
                        <Button outline theme="danger" className="mb-2 mr-1">
                            Escalade
                        </Button>
                        <Button outline theme="warning" className="mb-2 mr-1">
                            Voyage
                        </Button>
                        <Button outline theme="info" className="mb-2 mr-1">
                            Kayak
                        </Button>
                        <Button outline theme="dark" className="mb-2 mr-1">
                            Voile
                        </Button>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default Tags;
