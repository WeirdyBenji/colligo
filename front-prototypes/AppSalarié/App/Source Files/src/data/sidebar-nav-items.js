export default function() {
  return [
    {
      title: "Home",
      to: "/home",
      htmlBefore: '<i class="material-icons">edit</i>',
      htmlAfter: ""
    },
    {
      title: "Mes événements",
      htmlBefore: '<i class="material-icons">vertical_split</i>',
      to: "/my_events",
    },
    {
      title: "Préferences",
      htmlBefore: '<i class="material-icons">note_add</i>',
      to: "/preferences",
    }
  ];
}
