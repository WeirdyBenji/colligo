import Cookies from 'universal-cookie';

export const setUser = (user) => {
    const cookies = new Cookies();
    cookies.set('user', user, { path: '/' });
}

export const getUser = () => {
    const cookies = new Cookies();
    return cookies.get('user');
}

export const removeUser = () => {
    const cookies = new Cookies();
    cookies.remove('user', { path: '/' });
}