import React from "react";
import PropTypes from "prop-types";
import { Container, Row, Col } from "shards-react";

import MainFooter from "../components/layout/MainFooter";

const NoLayout = ({ children, noFooter }) => (
  <Container fluid>
    <Row>
      <Col
        className="main-content p-0"
        tag="main"
      >
        {children}
        {!noFooter && <MainFooter />}
      </Col>
    </Row>
  </Container>
);

NoLayout.propTypes = {
  /**
   * Whether to display the footer, or not.
   */
  noFooter: PropTypes.bool
};

NoLayout.defaultProps = {
  noFooter: false
};

export default NoLayout;