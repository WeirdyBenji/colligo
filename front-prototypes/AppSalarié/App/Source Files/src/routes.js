import React from "react";
import { Redirect } from "react-router-dom";

// Layout Types
import { DefaultLayout, NoLayout } from "./layouts";

// Route Views
import Home from './views/Home';
import MyEvents from './views/MyEvents';
import Preferences from './views/Preferences';
import SignIn from './views/SignIn';

export default [
  {
    path: "/",
    exact: true,
    layout: DefaultLayout,
    component: () => <Redirect to="/home" />
  },
  {
    path: "/sign_in",
    layout: NoLayout,
    component: SignIn
  },
  {
    path: "/home",
    layout: DefaultLayout,
    component: Home
  },
  {
    path: "/my_events",
    layout: DefaultLayout,
    component: MyEvents
  },
  {
    path: "/preferences",
    layout: DefaultLayout,
    component: Preferences
  },
];
