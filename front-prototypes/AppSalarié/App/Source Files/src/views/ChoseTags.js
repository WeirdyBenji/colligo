/* eslint jsx-a11y/anchor-is-valid: 0 */

import React from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  CardFooter,
  Badge,
  Button
} from "shards-react";

import PageTitle from "../components/common/PageTitle";

class ChoseTags extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      // First list of posts.
      PostsListOne: [],

      // Second list of posts.
      PostsListTwo: [],
    };
  }

  getActivities = () => {
    var xhr = new XMLHttpRequest()

    xhr.addEventListener('load', () => {
        let resultPublic = []
        let resultPrivate = []
        let res
        try {
            res = JSON.parse(xhr.responseText)
        } catch (e) {
            console.log(e.message)
            return
        }
        if (res.length === 0)
            this.setState({PostsList: []})
        res.forEach(activity => {
            let act = {
                backgroundImage: activity.backgroundImage,
                category: activity.category,
                categoryTheme: activity.theme,
                title: activity.title,
                body: activity.description,
                date: new Date(Date(activity.date)).toLocaleString(),
                participantNb: activity.numberParticipants,
                participantTotal: activity.maxParticipants,
                registeredParticipants: activity.registeredParticipants
            }
        console.log(act)
        if (activity.isPrivate && activity.registeredParticipants && !activity.registeredParticipants.includes('dlQPjC0uQdgBmieQgt6zH5lkVfA2'))
            resultPrivate.push(act)
        if (!activity.isPrivate && activity.registeredParticipants && !activity.registeredParticipants.includes('dlQPjC0uQdgBmieQgt6zH5lkVfA2'))
            resultPublic.push(act)

        this.setState({PostsListOne: resultPublic, PostsListTwo: resultPrivate})
        });
    })
    xhr.open('GET', 'http://localhost:3001/activity/')
    xhr.send()
  }

  UNSAFE_componentWillMount = () => {
    this.getActivities()
  }

  onClick = (e) => {
        var xhr = new XMLHttpRequest()
        xhr.addEventListener('load', () => {
            this.getActivities()
        })
        xhr.open('PATCH', `http://localhost:3001/users/dlQPjC0uQdgBmieQgt6zH5lkVfA2/activities/${e.target.value}`)
        xhr.send()
        e.preventDefault();
  }

  render() {
    const {
      PostsListOne,
      PostsListTwo,
    } = this.state;

    return (
        <div>
        <Container fluid className="main-content-container px-4">
            {/* Page Header */}
            <Row noGutters className="page-header py-4">
            <PageTitle sm="4" title="Les événements du moment !" subtitle="Publique" className="text-sm-left" />
            </Row>

            {/* First Row of Posts */}
            <Row>
            {PostsListOne.map((post, idx) => (
                <Col lg="3" md="6" sm="12" className="mb-4" key={idx}>
                <Card small className="card-post card-post--1">
                    <div
                    className="card-post__image"
                    style={{ backgroundImage: `url(${post.backgroundImage})` }}
                    >
                    <Badge
                        pill
                        className={`card-post__category bg-${post.categoryTheme}`}
                    >
                        {post.category}
                    </Badge>
                    </div>
                    <CardBody>
                    <h5 className="card-title">
                        <a href="#" className="text-fiord-blue">
                        {post.title}
                        </a>
                    </h5>
                    <p className="card-text d-inline-block mb-3">{post.body}</p>
                    <span className="text-muted">{post.date}</span>
                    </CardBody>
                    <div className="my-auto ml-auto">
                        <span className="text-muted">{`${post.participantNb}/${post.participantTotal}`}</span>
                        <Button value={post.title} onClick = {this.onClick} size="sm" theme="white">
                            <i className="far fa-bookmark mr-1" /> Rejoindre
                        </Button>
                    </div>
                </Card>
                </Col>
            ))}
            </Row>
        </Container>

        <Container fluid className="main-content-container px-4">
            {/* Page Header */}
            <Row noGutters className="page-header py-4">
            <PageTitle sm="4" title="Les événements privés" subtitle="Privé" className="text-sm-left" />
            </Row>

            {/* First Row of Posts */}
            <Row>
            {PostsListTwo.map((post, idx) => (
                <Col lg="3" md="6" sm="12" className="mb-4" key={idx}>
                <Card small className="card-post card-post--1">
                    <div
                    className="card-post__image"
                    style={{ backgroundImage: `url(${post.backgroundImage})` }}
                    >
                    <Badge
                        pill
                        className={`card-post__category bg-${post.categoryTheme}`}
                    >
                        {post.category}
                    </Badge>
                    </div>
                    <CardBody>
                    <h5 className="card-title">
                        <a href="#" className="text-fiord-blue">
                        {post.title}
                        </a>
                    </h5>
                    <p className="card-text d-inline-block mb-3">{post.body}</p>
                    <span className="text-muted">{post.date}</span>
                    </CardBody>
                    <div className="my-auto ml-auto">
                        <span className="text-muted">{`${post.participantNb}/${post.participantTotal}`}</span>
                        <Button value={post.title} onClick = {this.onClick} size="sm" theme="white">
                        <i className="far fa-bookmark mr-1" /> Rejoindre
                        </Button>
                    </div>
                </Card>
                </Col>
            ))}
            </Row>
        </Container>
      </div>
    );
  }
}

export default ChoseTags;
