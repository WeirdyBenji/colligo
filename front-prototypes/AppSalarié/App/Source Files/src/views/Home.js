/* eslint jsx-a11y/anchor-is-valid: 0 */

import React from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  CardFooter,
  Badge,
  Button
} from "shards-react";

import PageTitle from "../components/common/PageTitle";

import { getUser } from '../helper/UserProfile'
import { apiProdUrl } from '../config'

class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
        apiUrl: apiProdUrl,
        user: getUser(),
      // First list of posts.
      PostsListOne: [],

      // Second list of posts.
      PostsListTwo: [],
    };
  }

  getActivities = () => {
    var xhr = new XMLHttpRequest()

    xhr.addEventListener('load', () => {
        let resultPublic = []
        let resultPrivate = []
        let res
        try {
            res = JSON.parse(xhr.responseText)
        } catch (e) {
            console.log(e.message)
            return
        }
        if (res.length === 0)
            this.setState({PostsList: []})
        res.forEach(activity => {
            let act = {
                backgroundImage: activity.image,
                category: activity.category,
                categoryTheme: activity.theme,
                title: activity.title,
                body: activity.description,
                date: new Date(Date(activity.date)).toLocaleString(),
                participantNb: activity.numberParticipants,
                participantTotal: activity.maxParticipants,
                registeredParticipants: activity.registeredParticipants ? activity.registeredParticipants : [],
                isPrivate: activity.isPrivate,
                id: activity.id
            }
        console.log(act)
        if (act.title != '-1') {
            if (act.isPrivate && !act.registeredParticipants.includes(this.state.user.id))
                resultPrivate.push(act)
            if (!act.isPrivate && !act.registeredParticipants.includes(this.state.user.id))
                resultPublic.push(act)
        }

        this.setState({PostsListOne: resultPublic, PostsListTwo: resultPrivate})
        });
    })
    xhr.open('GET', `${this.state.apiUrl}/activity/`)
    xhr.send()
  }

  UNSAFE_componentWillMount = () => {
    this.getActivities()
  }

  onClick = (e) => {
        var xhr = new XMLHttpRequest()
        xhr.addEventListener('load', () => {
            this.getActivities()
        })
        xhr.open('PATCH', `${this.state.apiUrl}/users/${this.state.user.id}/activities/${e.target.value}`)
        console.log(`${this.state.apiUrl}/users/${this.state.user.id}/activities/${e.target.value}`)
        xhr.send()
        e.preventDefault();
  }

  render() {
    const {
      PostsListOne,
      PostsListTwo,
    } = this.state;

    return (
        <Container fluid className="main-content-container px-4">
        <Container fluid className="px-4">
            {/* Page Header */}
            <Row noGutters className="page-header py-4">
            <PageTitle sm="4" title="Les événements du moment !" subtitle="Publique" className="text-sm-left" />
            </Row>

            {/* First Row of Posts */}
            <Row>
            {PostsListOne.length > 0 ? PostsListOne.map((post, idx) => (
                <Col lg="3" md="6" sm="12" className="mb-4" key={idx}>
                <Card small className="card-post card-post--1">
                    <div
                    className="card-post__image"
                    style={{ backgroundImage: `url(${post.backgroundImage})` }}
                    >
                    <Badge
                        pill
                        className={`card-post__category bg-${post.categoryTheme}`}
                    >
                        {post.category}
                    </Badge>
                    </div>
                    <CardBody>
                    <h5 className="card-title">
                        <a href="#" className="text-fiord-blue">
                        {post.title}
                        </a>
                    </h5>
                    <p className="card-text d-inline-block mb-3">{post.body}</p>
                    <span className="text-muted">{post.date}</span>
                    </CardBody>
                    <div className="my-auto ml-auto">
                        <span className="text-muted">{`${post.participantNb}/${post.participantTotal}`}</span>
                        <Button value={post.id} onClick = {this.onClick} size="sm" theme="white">
                            <i className="far fa-bookmark mr-1" /> Rejoindre
                        </Button>
                    </div>
                </Card>
                </Col>
            ))
            : <Col> Aucun événement </Col>}
            </Row>
        </Container>

        <Container fluid className="px-4">
            {/* Page Header */}
            <Row noGutters className="page-header py-4">
            <PageTitle sm="4" title="Les événements privés" subtitle="Privé" className="text-sm-left" />
            </Row>

            {/* First Row of Posts */}
            <Row>
            {PostsListTwo.map((post, idx) => (
                <Col lg="3" md="6" sm="12" className="mb-4" key={idx}>
                <Card small className="card-post card-post--1">
                    <div
                    className="card-post__image"
                    style={{ backgroundImage: `url(${post.backgroundImage})` }}
                    >
                    <Badge
                        pill
                        className={`card-post__category bg-${post.categoryTheme}`}
                    >
                        {post.category}
                    </Badge>
                    </div>
                    <CardBody>
                    <h5 className="card-title">
                        <a href="#" className="text-fiord-blue">
                        {post.title}
                        </a>
                    </h5>
                    <p className="card-text d-inline-block mb-3">{post.body}</p>
                    <span className="text-muted">{post.date}</span>
                    </CardBody>
                    <div className="my-auto ml-auto">
                        <span className="text-muted">{`${post.participantNb}/${post.participantTotal}`}</span>
                        <Button value={post.id} onClick = {this.onClick} size="sm" theme="white">
                        <i className="far fa-bookmark mr-1" /> Rejoindre
                        </Button>
                    </div>
                </Card>
                </Col>
            ))}
            </Row>
        </Container>
      </Container>
    );
  }
}

export default Home;
