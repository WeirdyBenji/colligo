/* eslint jsx-a11y/anchor-is-valid: 0 */

import React from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  CardFooter,
  Badge,
  Button
} from "shards-react";

import PageTitle from "../components/common/PageTitle";

import { getUser } from '../helper/UserProfile'
import { apiProdUrl } from '../config'

class MyEvents extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
        apiUrl: apiProdUrl,
        user: getUser(),
      // Second list of posts.
      PostsList: [],
    };
  }

  getActivities = () => {
    this.setState({PostsList: []})
    var xhr = new XMLHttpRequest()

    try {
        xhr.addEventListener('load', () => {
            let activities = {}

            try {
                activities = JSON.parse(xhr.responseText)
            } catch (e) {
                console.log(e.message)
                return
            }

            console.log(activities)
            if (activities.length === 0) {
                this.setState({PostsList: []})
                return
            }

            activities.forEach(name => {
                var xhr2 = new XMLHttpRequest()
                xhr2.addEventListener('load', () => {
                    let result = this.state.PostsList;
                    let res
                    try {
                        res = JSON.parse(xhr2.responseText)
                    } catch (e) {
                        console.log(e.message)
                        return
                    }
                    let act = {
                        backgroundImage: res.image,
                        category: res.category,
                        categoryTheme: res.theme,
                        title: res.title,
                        body: res.description,
                        date: new Date(Date(res.date)).toLocaleString(),
                        participantNb: res.numberParticipants,
                        participantTotal: res.maxParticipants,
                        id: res.id
                    }
                    result.push(act);
                    this.setState({PostsList: result})
                });
                xhr2.open('GET', `${this.state.apiUrl}/activity/${name}`)
                xhr2.send()
            })
        })
        xhr.open('GET', `${this.state.apiUrl}/users/${this.state.user.id}/activities`)
        xhr.send()
    } catch (e) {
        console.log(e.message)
    }
  }

  UNSAFE_componentWillMount = () => {
    this.getActivities()
  }

  onClick = (e) => {
    var xhr = new XMLHttpRequest()
    xhr.addEventListener('load', () => {
        this.getActivities()
    })
    xhr.open('DELETE', `${this.state.apiUrl}/users/${this.state.user.id}/activities/${e.target.value}`)
    xhr.send()
    e.preventDefault();
  }


  render() {
    const {
        PostsList
    } = this.state;

    return (
            <Container fluid className="main-content-container px-4">
                {/* Page Header */}
                <Row noGutters className="page-header py-4">
                    <PageTitle sm="4" title="Mes événements" className="text-sm-left" />
                </Row>

                <Row>
                {PostsList.length > 0 ? PostsList.map((post, idx) => (
                    <Col lg="3" md="6" sm="12" className="mb-4" key={idx}>
                    <Card small className="card-post card-post--1">
                        <div
                        className="card-post__image"
                        style={{ backgroundImage: `url(${post.backgroundImage})` }}
                        >
                        <Badge
                            pill
                            className={`card-post__category bg-${post.categoryTheme}`}
                        >
                            {post.category}
                        </Badge>
                        </div>
                        <CardBody>
                        <h5 className="card-title">
                            <a href="#" className="text-fiord-blue">
                            {post.title}
                            </a>
                        </h5>
                        <p className="card-text d-inline-block mb-3">{post.body}</p>
                        <span className="text-muted">{post.date}</span>
                        </CardBody>
                        <div className="my-auto ml-auto">
                            <span className="text-muted">{`${post.participantNb}/${post.participantTotal}`}</span>
                            <Button value={post.id} onClick = {this.onClick} size="sm" theme="white">
                            <i className="far fa-bookmark mr-1" /> Quitter
                            </Button>
                        </div>
                    </Card>
                    </Col>
                ))
                : <Col> Aucun événement </Col>}
                </Row>
            </Container>
    );
  }
}

export default MyEvents;
