import React, { Component } from "react";
import { Container, Row, Col, Card, CardHeader, CardBody } from "shards-react";

import PageTitle from "../components/common/PageTitle";
import Tags from "../components/components-overview/Tags";
import EventCalendar from "../components/EventCalendar.js"

import { apiProdUrl } from '../config'

class Preferences extends Component {
    constructor(props) {
        super(props)

        this.state = {
            tags: [
                /*{
                    name: 'Laser-game',
                    color: 'secondary'
                },
                {
                    name: 'Escape Games',
                    color: 'success'
                }*/
            ]
        }
    }

    getTags = () => {
        var xhr = new XMLHttpRequest()
        xhr.addEventListener('load', () => {
            console.log(xhr.responseText)
            let tags = [{
                name: 'Laser-game',
                color: 'secondary'
            },
            {
                name: 'Escape Games',
                color: 'success'
            }]
            this.setState({tags: tags})
        })
        xhr.open('GET', `${apiProdUrl}/users/dlQPjC0uQdgBmieQgt6zH5lkVfA2/tags`)
        xhr.send()
    }

    UNSAFE_componentWillMount = () => {
        this.getTags()
    }

    render = () => (
        <Container fluid className="main-content-container px-4">
            <Row noGutters className="page-header py-4">
                <Col>
                    <PageTitle
                        md="12"
                        className="ml-sm-auto mr-sm-auto"
                        title="Mon Planning"
                        />
                </Col>
            </Row>
            <Row>
                <Col>
                    <Card>
                        <CardHeader>
                        </CardHeader>
                        <CardBody>
                            <EventCalendar />
                        </CardBody>
                    </Card>
                </Col>
            </Row>
            <Row noGutters className="page-header py-4">
                <Card>
                    <CardHeader>
                    </CardHeader>
                    <CardBody>
                        <Tags tags={this.state.tags}
                        />
                    </CardBody>
                </Card>
            </Row>
        </Container>
    );
};

export default Preferences;
