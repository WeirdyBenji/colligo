import React, { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Snackbar from "../components/Snackbar/Snackbar.js";
import { makeStyles } from '@material-ui/core/styles';

import { setUser, getUser } from '../helper/UserProfile'
import { apiProdUrl } from '../config'

const INITIAL_STATE = {
  email: '',
  password: '',
  error: null,
};


const useStyles = makeStyles(theme => ({
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: 'url(https://images.unsplash.com/photo-1506744038136-46273834b3fb?ixlib=rb-1.2.1&w=1000&q=80)',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const SignInPage = props => {
  const classes = useStyles();
  const [state, setState] = useState({...INITIAL_STATE});
  const [toHome, setRedirState] = useState(false);
  const [tc, setTC] = useState(false);
  const [error, setError] = useState("");


  const logIn = (email, password) => {
    var xhr = new XMLHttpRequest();

    try {
        xhr.open('POST', `${apiProdUrl }/login/`)
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.onload = function() {
            let user = JSON.parse(xhr.responseText)
            console.log(user)
            if (user && user.token) {
                setUser(user)
                setRedirState(true)
            } else {
                setError(user.message)
                setTC(true);
                setTimeout(function() {
                    setTC(false);
                }, 6000);
            }
        }

        xhr.send(JSON.stringify({email, password}));
    } catch (e) {
        console.log(e.message)
    }
}

useEffect(() => {
    if (getUser()) {
        setRedirState(true)
    }
})

  const onSubmit = event => {
    const {email, password} = state
    logIn(email, password)
    event.preventDefault();
  }

  const onChange = event => {
    const { name, value } = event.target;
    setState(prevState => ({ ...prevState, [name]: value }));
  };

  return (
    <Grid container component="main" className={classes.root}>
    {toHome ? <Redirect to="/" /> : null}
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <form className={classes.form}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              value={state.email}
              onChange={onChange}
              autoComplete="email"
              autoFocus
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              value={state.password}
              onChange={onChange}
              type="password"
              id="password"
              autoComplete="current-password"
            />
            <Snackbar
              place="tc"
              color="danger"
              message={error}
              open={tc}
              closeNotification={() => setTC(false)}
              close
            />
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              disabled={state.password === "" || state.email === ""}
              onClick={onSubmit}
              className={classes.submit}
            >
              Sign In
            </Button>
          </form>
        </div>
      </Grid>
    </Grid>
  );
};

export default SignInPage;