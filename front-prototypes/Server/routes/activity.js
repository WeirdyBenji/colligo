const express = require('express');
const router = express.Router();
const db = require("../server").db;
const admin = require("../server").admin;

// GET /activity
router.get("/", (req, res) => {
    db.collection("activities").get()
        .then((snapshot) => {
            let data = [];
            snapshot.forEach((doc) => {
                data.push(doc.data());
            });
            res.json(data);
        })
        .catch((err) => {
            console.log("GET Request Errors ", err);
        });
});

// GET /activity/:id
router.get("/:id", async (req, res) => {
    let id = req.params.id;
    console.log(id);
    db.collection("activities").doc(id).get()
        .then((snapshot) => {
            let data = [];
            console.log(snapshot.data());
            res.json(snapshot.data());
        }).catch((err) => {
            console.log("Activity doesn't exist ", err);
    });
});

// POST /activity
router.post("/", (req, res) => {
    // console.log(req.body);
    let id = req.body.id;
    let isPrivate = req.body.isPrivate;
    let name = req.body.title;
    let imgURL = req.body.backgroundImage;
    let category = req.body.category;
    let description = req.body.description;
    let date = new Date(req.body.date);
    let participants = req.body.numberParticipants;
    let maxParticipants = req.body.maxParticipants;
    let theme = req.body.theme;
    let attendees = req.attendees;

    db.collection("activities").doc().set({
        "id": id,
        "isPrivate": isPrivate,
        "backgroundImage": imgURL,
        "category": category,
        "title": name,
        "description": description,
        "date": date,
        "attendees" : attendees,
        "numberParticipants": participants,
        "maxParticipants": maxParticipants,
        "theme": theme
    });
    res.end("201");
});

module.exports = router;