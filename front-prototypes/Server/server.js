// REQUIRES
const express = require("express");
const bodyParser = require("body-parser");
const admin = require("firebase-admin");
const serviceAccount = require('./firebase');
const firebase = require("firebase");
const cors = require('cors');

const PORT = 3001;
const app = express();

app.use(cors());

// SET CORS
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// Firebase Config
firebase.initializeApp({
    apiKey: "AIzaSyCyo2POliv8AJy-A7ns9-fKICmFi86UMrI",
    authDomain: "eip-colligo.firebaseapp.com",
    databaseURL: "https://eip-colligo.firebaseio.com",
    projectId: "eip-colligo",
    storageBucket: "eip-colligo.appspot.com",
    messagingSenderId: "229987747765",
    appId: "1:229987747765:web:c3d3e7812d6b068656bbfc",
    measurementId: "G-ZQL9PKPY0R"
});

module.exports.firebase = firebase;

// Firebase Admin Config
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    // databaseURL: "https://eip-colligo.firebaseio.com"
});

// Exports Firebase variables
module.exports.admin = admin;
module.exports.db  = admin.firestore();

// Swagger UI Express Config
const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const swaggerDocument = YAML.load('./swagger.yaml');
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// BodyParser Config
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// GET Routers
const indexRouter = require("./routes/index");
const userRouter = require("./routes/users");
const activityRouter = require("./routes/activity");
const companyRouter = require("./routes/company");
const tagsRouter = require("./routes/tags");

// SET Routers
app.use("/", indexRouter);
app.use("/users", userRouter);
app.use("/activity", activityRouter);
app.use("/company", companyRouter);
app.use("/tags", tagsRouter);

app.listen(PORT, () => {
    console.log(`Server is listening to PORT: ${PORT}`);
});

module.exports = app;
