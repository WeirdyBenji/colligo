#!/usr/bin/env python3  

import urllib.request, json
import smtplib
import sys
from string import Template

mail_list = []
name_list = []

def read_template(filename):
    with open(filename, 'r', encoding='utf-8') as template_file:
        template_file_content = template_file.read()
    return Template(template_file_content)


#main
s = smtplib.SMTP(host='smtp.gmail.com', port=587)
s.starttls()
#s.login("lumsrouge@gmail.com", "epitech2021")
s.login("colligo.event@gmail.com", "tesbeau0k")

with urllib.request.urlopen(sys.argv[1]) as url:
    data = json.loads(url.read().decode())
    for employees in data:
        mail_list.append(employees['email'])
        name_list.append(employees['email'].split("@")[0])

print(mail_list)
print(name_list)

message_template = read_template('message_template.txt')


from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

for name, email in zip(name_list, mail_list):
    print("Sending mail to ...")
    print(email)
    msg = MIMEMultipart()
    message = message_template.substitute(NAME=name.title(),
    EVENT_NAME=sys.argv[2], LINK="link to precise")
    msg['From']="colligo.event@gmail.com"
    msg['To']=email
    msg['Subject']="You have been invited to an event!"
    msg.attach(MIMEText(message, 'plain'))
    s.send_message(msg)    
    del msg