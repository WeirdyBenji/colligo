const express = require('express');
const router = express.Router();
const db = require("../server").db;
const admin = require("../server").admin;

// GET /company
router.get("/", (req, res) => {
    db.collection("companies").get()
        .then((snapshot) => {
            let data = [];
            snapshot.forEach((doc) => {
                data.push(doc.data());
            });
            res.json(data);
        })
        .catch((err) => {
            console.log("GET Request Errors ", err);
        });
});

module.exports = router;