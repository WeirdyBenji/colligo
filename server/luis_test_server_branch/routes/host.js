const express = require('express');
const router = express.Router();
const db = require("../server").db;
const admin = require("../server").admin;

router.get("/", async (req, res) => {
    db.collection("hosts").get()
        .then((snapshot) => {
            let data = [];
            snapshot.forEach((doc) => {
                data.push(doc.data());
            });
            res.json(data);
        })
        .catch((err) => {
            console.log("err ", err);
        })
});

router.get("/:id", async  (req, res) => {
    let id = req.params.id;
    db.collection("hosts").doc(id).get()
        .then((snapshot) => {
            res.json(snapshot.data());
        })
        .catch((err) => {
            console.log("Tag id doesn't exist ", err);
        });
});

module.exports = router;