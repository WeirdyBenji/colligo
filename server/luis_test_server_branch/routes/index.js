const express = require('express');
const router = express.Router();
const db = require("../server").db;
const admin = require("../server").admin;
const firebase = require("../server").firebase;

// POST /login
router.post("/login", (req, res) => {
    // console.log(req.body);
    firebase.auth().signInWithEmailAndPassword(req.body.email, req.body.password)
        .then((user) => {
            // console.log("USER: ", user.user.id);
            // res.json(user);
            user.user.getIdToken(true)
                .then((token) => {
                    res.end(JSON.stringify({token:token}))
                })
                .catch((err) => {
                    console.log("User token failed , ", err)
                });
        })
        .catch((err) => {
            // console.log("Err , ", err);
            // res.writeHead(500, {"Content-Type": "application/json"});
            res.end(JSON.stringify({code: err.code, message: err.message}));
        });
    // res.end("cool");
});

router.post("/register", (req, res) => {
    console.log(req.body);
    admin.auth().createUser({
        email: req.body.email,
        emailVerified: false,
        password: req.body.password,
        displayName: 'noname',
        disabled: false
        }).then(function(userRecord) {
          // See the UserRecord reference doc for the contents of userRecord.
          console.log('Successfully created new user:', userRecord.uid);
          db.collection("users").doc(userRecord.uid).set({
                "activities" : [],
                "compagny" : "ECorp",
                "email" : req.body.email,
                "firstName" : "John",
                "lastName" : "Doe",
                "interests" : ["cookies"],
                "type" : "-1"
          });
          res.end("Succesfully registered!");
        })
        .catch((err) => {
            // console.log("Err , ", err);
            // res.writeHead(500, {"Content-Type": "application/json"});
            console.log('Error creating new user:', err);
            res.end(JSON.stringify({code: err.code, message: err.message}));
        });
    
});

module.exports = router;