const express = require('express');
const router = express.Router();
const db = require("../server").db;
const admin = require("../server").admin;

// GET /users
router.get("/", (req, res) => {
    db.collection("users").get()
        .then((snapshot) => {
            let data = [];
            snapshot.forEach((doc) => {
                data.push(doc.data());
            });
            res.json(data);
        })
        .catch((err) => {
            console.log("GET Request Errors ", err);
        });
});

// GET /user/:id
router.get("/:id", async (req, res) => {
    let id = req.params.id;
    db.collection("users").doc(id).get()
        .then((snapshot) => {
            res.json(snapshot.data());
        })
        .catch((err) => {
            console.log("User id doesn't exist ", err);
        });
});

//  GET tags
router.get("/:id/tags", async (req, res) => {
    let id = req.params.id;
    db.collection("users").doc(id).get()
        .then((snapshot) => {
            res.json(snapshot.get("interests"));
        })
        .catch((err) => {
            console.log("Err ", err);
        });
});

//  PATCH tags
router.patch("/:id/tags/", async (req, res) => {
    let id = req.params.id;
    console.log(req.body);
    db.collection("users").doc(id).update({
        interets: req.body.interests
    }).then((snap) => {
        res.end("202")
    }).catch((err) => {
        console.log("Errr , ", err)
    })
    // let tagid = req.params.tagid;
   // db.collection("users").doc(id).update({
   //     interets: admin.firestore.FieldValue.arrayUnion(tagid)
   // }).then((snap) => {
   //     res.end("202")
   // }).catch((err) => {
   //     console.log("400 , ", err)
   // })
});
//
// //  DELETE tags
// router.delete("/:id/tags/:tagid", async (req, res) => {
//     let id = req.params.id;
//     let tagid = req.params.tagid;
//     db.collection("users").doc(id).update({
//         interets: admin.firestore.FieldValue.arrayRemove(tagid)
//     }).then((snap) => {
//         res.end("202")
//     }).catch((err) => {
//         console.log("400 , ", err)
//     })
// });

// GET /user/:id/activities
router.get("/:id/activities", async (req, res) => {
    let id = req.params.id;
    db.collection("users").doc(id).get()
        .then((snapshot) => {
            res.json(snapshot.get("registeredActivities"));
        })
        .catch((err) => {
            console.log("GET request error ", err);
        });
});

// PATCH /user/:id/activities/:activityId
router.patch("/:id/activities/:activityId", (req, res) => {
    let id = req.params.id;
    let activityId = req.params.activityId;
    db.collection("activities").doc(activityId).update({
        registeredParticipants: admin.firestore.FieldValue.arrayUnion(id)
    });
    db.collection("users").doc(id).update({
        registeredActivities: admin.firestore.FieldValue.arrayUnion(activityId)
    })
        .then((snapshot) => {
            res.end("202");
        })
        .catch((err) => {
            console.log("Err ", err);
        })
});

// DELETE /user/:id/activities/:activityId
router.delete("/:id/activities/:activityId", (req, res) => {
    let id = req.params.id;
    let activityId = req.params.activityId;
    db.collection("activities").doc(activityId).update({
        registeredParticipants: admin.firestore.FieldValue.arrayRemove(id)
    });
    db.collection("users").doc(id).update({
        registeredActivities: admin.firestore.FieldValue.arrayRemove(activityId)
    })
        .then((snapshot) => {
            res.end("202");
        })
        .catch((err) => {
            console.log("Err ", err);
        })
});

// POST /users
router.post("/", (req, res) => {
    // console.log(req.body);
    db.collection("users").doc().set({
        "email": req.body.email,
        "first": req.body.first,
        "interests": req.body.interests,
        "last": req.body.last,
        "type": req.body.type,
        "registeredActivities": eq.body.registeredActivities
    });
    res.end("201");
});

module.exports = router;