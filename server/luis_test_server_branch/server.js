// REQUIRES
const express = require("express");
const bodyParser = require("body-parser");
const admin = require("firebase-admin");
const firebase = require("firebase");
const app = express();
const PORT = 3000;

let serviceAccount = require('../../firebase-credentials');

// Firebase Config
firebase.initializeApp({
    apiKey: "AIzaSyCyo2POliv8AJy-A7ns9-fKICmFi86UMrI",
    authDomain: "eip-colligo.firebaseapp.com",
    databaseURL: "https://eip-colligo.firebaseio.com",
    projectId: "eip-colligo",
    storageBucket: "eip-colligo.appspot.com",
    messagingSenderId: "229987747765",
    appId: "1:229987747765:web:c3d3e7812d6b068656bbfc",
    measurementId: "G-ZQL9PKPY0R"
});

module.exports.firebase = firebase;

// Firebase Admin Config
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://nth-avatar-261115.firebaseio.com"
});

const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const swaggerDocument = YAML.load('../server/api/swagger-openapi.yaml');

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));


let db = admin.firestore();

// Exports Firebase variables
module.exports.admin = admin;
module.exports.db  = admin.firestore();

// BodyParser Config
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// GET Routers
const userRouter = require("./routes/users");
const activityRouter = require("./routes/activity");
const companyRouter = require("./routes/company");

// SET Routers
app.use("/users", userRouter);
app.use("/activity", activityRouter);
app.use("/company", companyRouter);

// SET CORS
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.listen(PORT, () => {
    console.log(`Server is listening to PORT: ${PORT}`);
});

module.exports = app;